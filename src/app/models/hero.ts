/**
 * Hero class to be used in the AppComponent
 */
export class Hero {
    id: number;
    name: string;
}
export default Hero;
