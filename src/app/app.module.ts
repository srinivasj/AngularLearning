import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// FormModule is used for two way binding using ngModel
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// parent components
import { AppComponent } from './app.component';

// child components
import { HeroesComponent } from './heroes/heroes.component';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { NotFoundComponent } from './not-found/not-found-component';
import { DxButtonComponent } from './dx-button/dx-button.component';

// services
import { HeroService } from './services/hero.service';

// libraries - third party
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DxButtonModule } from 'devextreme-angular';

// routes - import routes defined in route.ts
import routes from './routes';

/**
 * Create routing for the application
 */

/**
 * Decalares neccessary imports and components for an app
 */
@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroListComponent,
    HeroDetailComponent,
    NotFoundComponent,
    DxButtonComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    AngularFontAwesomeModule,
    DxButtonModule,
  ],
  providers: [
    HeroService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
