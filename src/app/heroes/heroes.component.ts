import { Component, OnInit } from '@angular/core';
import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';


@Component({
    selector: 'app-heroes',
    template: `<!--The content below is only a placeholder and can be replaced.-->
                <div class="container">
                <h3>
                    Welcome to {{title}} {{hero && hero.name}}
                </h3>
                <app-hero-list [data_loading]='data_loading' [heroes]='heroes'
                    ([ngModel])="hero" (heroChange)="changeHero($event)"></app-hero-list>
                <app-hero-detail [hero]="hero"></app-hero-detail>
                </div>`
})

export class HeroesComponent implements OnInit {
    constructor(private heroService: HeroService) {}
    title: String = 'All components in one page! Heroes - ';
    hero: Hero = null;
    status: String = 'Edit';
    heroes: Hero[];
    data_loading: Boolean = true;
    getHeroes = () => {
        this.heroService.getHeroesDelay().then(heroes => { this.heroes = heroes; this.data_loading = false; });
    }
    changeHero = (hero: Hero) => {
        this.hero = hero;
    }
    ngOnInit(): void {
        this.getHeroes();
    }
}
