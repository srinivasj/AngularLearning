import { Component } from '@angular/core';

@Component({
    template:   `<h3>Not Found<h3>
                <p>{{title}}</p>`
})

export class NotFoundComponent {
    title = 'Are you trying to access wrong page?';
}
