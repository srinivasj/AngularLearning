import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

/**
 * @Component specifies a basic component that is declared with class
 * selector - which html tag corresponds to this component
 * template - html that actually is rendered for this component - can be multi line with ``
 * templateUrl - html file that is rendered with this component
 * styleUrls - an array of css files that have styling with this component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [],
})

/**
 * AppComponent is the base/parent Component that is used to render all other components
 * below class is a component that contains the data that needs to be dynamic or used in multiple places
 */
export class AppComponent implements OnInit {
  constructor(private router: Router, private location: Location) {}
  ngOnInit() {
    // this.href = this.router.url;
    // console.log(this.router.url, this.location.path());
  }
}
