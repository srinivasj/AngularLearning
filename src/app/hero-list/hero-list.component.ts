import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
/**
 * @Component specifies a basic component that is declared with class
 * selector - which html tag corresponds to this component
 * template - html that actually is rendered for this component - can be multi line with ``
 * templateUrl - html file that is rendered with this component
 * styleUrls - an array of css files that have styling with this component
 */
@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css'],
})


/**
 * HeroListComponent used to show the list of hero componenets
 * Assign all inputs, incoming attributes to local instances
 */
export class HeroListComponent implements OnInit {
  constructor(private heroService: HeroService, 
    private location: Location,
    private router: Router) {

  }
  title = 'Tour of Heroes';
  @Input() hero;
  @Input() heroes: Hero[];
  @Input() data_loading: Boolean = true;
  @Output() heroChange: EventEmitter<Hero> = new EventEmitter();
  list_page: Boolean = false;
  onSelect(hero: Hero): void {
    this.hero = hero;
    if (!this.list_page) {
      this.heroChange.emit(this.hero);
    } else if (this.hero) {
      this.router.navigate(['/detail_heroes', this.hero.id]);
      console.log(this.hero);
    }
  }
  getHeroes = () => {
    this.heroService.getHeroesDelay().then(heroes => { this.heroes = heroes;       this.data_loading = false; });
  }
  ngOnInit() {
    if (this.location.path().indexOf('list_heroes') > 0) {
      this.getHeroes();
      this.list_page = true;
    }
  }
}
