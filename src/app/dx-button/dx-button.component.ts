import { Component } from '@angular/core';


@Component({
    selector: 'app-dx',
    template: '<dx-button text="Press me" (onClick)="helloWorld()"></dx-button>'
})
export class DxButtonComponent {
    helloWorld() {
        alert('Hello world!');
    }
}