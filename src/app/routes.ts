import { Routes } from '@angular/router';

// all components components
import { HeroesComponent } from './heroes/heroes.component';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { NotFoundComponent } from './not-found/not-found-component';
import { DxButtonComponent } from './dx-button/dx-button.component';

const routes: Routes = [
    {
      path: '',
      redirectTo: '/heroes', pathMatch: 'full'
    },
    {
      path: 'heroes',
      component: HeroesComponent,
    },
    {
      path: 'list_heroes',
      component: HeroListComponent,
    },
    {
      path: 'detail_heroes/:id',
      component: HeroDetailComponent,
    },
    {
      path: 'dx-button',
      component: DxButtonComponent,
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ];

export default routes;
