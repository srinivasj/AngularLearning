import { Injectable } from '@angular/core';
import Hero from '.././models/hero';
import { HEROES } from './hero-mock-data';

/**
 * The HeroService could get Hero data from anywhere—a web service, local storage, or a mock data source.
 * Removing data access from the component means you can change your mind about the implementation anytime,
 * without touching the components that need hero data.
 */
@Injectable()
export class HeroService {
    getHeroes(): Promise<Hero[]> {
        return Promise.resolve(HEROES);
    }
    getHeroesDelay(): Promise<Hero[]> {
        return new Promise((resolve) => { setTimeout(() => resolve(this.getHeroes()), 500); });
    }
    getHeroesById(id: Number): Promise<Hero> {
        for (const i in HEROES) {
            if (HEROES[i].id === id) {
                return Promise.resolve(HEROES[i]);
            }
        }
        return null;
    }
}

