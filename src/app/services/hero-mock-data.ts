import Hero from '.././models/hero';

/**
 * create a dummy array for rendering into the list of heroes
 */

export const HEROES: Hero[] = [
    { id: 1, name: 'Batman' },
    { id: 2, name: 'Ironman' },
    { id: 3, name: 'Superman' },
    { id: 4, name: 'Flash' },
    { id: 5, name: 'Hulk' },
    { id: 6, name: 'Thor' },
    { id: 7, name: 'Wonder Women' },
    { id: 8, name: 'Aquamen' },
    { id: 9, name: 'Justice League' },
    { id: 10, name: 'Avengers' },
   ];
