import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

/**
 * @Component specifies a basic component that is declared with class
 * selector - which html tag corresponds to this component
 * template - html that actually is rendered for this component - can be multi line with ``
 * templateUrl - html file that is rendered with this component
 * styleUrls - an array of css files that have styling with this component
 */
@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css'],
})

/**
 * AppComponent is the base/parent Component that is used to render all other components
 * below class is a component that contains the data that needs to be dynamic or used in multiple places
 */
export class HeroDetailComponent implements OnInit {
  constructor(private location: Location,
        private heroService: HeroService,
        private route: ActivatedRoute) {  }
  @Input() hero;
  status: String = 'Edit';
  id: Number = 0;
  changeEdit = (event) => {
    if (this.status === 'Edit') {
      this.status = 'Done';
    } else {
      this.status = 'Edit';
    }
  }
  ngOnInit() {
    if (this.location.path().indexOf('detail_heroes') > 0) {
      this.route.params.subscribe(params => {
        this.id = +params['id']; // (+) converts string 'id' to a number
        this.heroService.getHeroesById(this.id).then(hero => this.hero = hero);
     });
    }
  }
}
